Pod::Spec.new do |s|

  s.name         = "MEContextArchiver"
  s.version      = "0.5.2"
  s.summary      = "MEContextCoding/MEContextArchiver save object context on disk without coding lines for NSCoding."

  s.homepage     = "git@bitbucket.org:denn_nevera/mecontextarchiver.git"

  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }
  s.author       = { "denis svinarchuk" => "denn.nevera@gmail.com" }
  s.platform     = :ios, '8.0'

  s.source       = { :git => "git@bitbucket.org:denn_nevera/mecontextarchiver.git", :tag => s.version}

  s.source_files  = 'MEContextArchiver/**/*.{h,m}'
  s.public_header_files = 'MEContextArchiver/*.h'

  s.requires_arc = true

end
