//
//  MEContextArchiver.m
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEContextArchiver.h"

static MEContextArchiver *__shared_instance = nil;
static dispatch_once_t onceToken;

@implementation MEContextArchiver
{
    NSString             *bundlePathToRecource;
    NSString             *workingPathToRecource;
    NSMutableDictionary  *dictionary;
}

+ (id) sharedInstanceWithFile:(NSString*)fileName{
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEContextArchiver alloc] initWithFileName:fileName];
    });
    
    return __shared_instance;
}

+ (id) sharedInstance{
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEContextArchiver alloc] init];
    });
    return __shared_instance;
}

- (id) initWithFileName:(NSString*)fileName{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    if (self) {
        workingPathToRecource = fileName;
        [self installBundle];
    }
    return self;
}

- (id) init{
    
    if (__shared_instance) {
        self = __shared_instance;
        return self;
    }
    
    self = [super init];
    if (self) {
        workingPathToRecource = ME_DEFAULT_CONTEXT_PLIST_NAME;
        [self installBundle];
    }
    return self;
}

- (void) installBundle{
    @synchronized(self){
        
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:[[NSLocale preferredLanguages] objectAtIndex:0]];
        
        bundlePathToRecource = [[NSBundle mainBundle] pathForResource:workingPathToRecource ofType:nil];
        workingPathToRecource = [documentsDirectory stringByAppendingPathComponent:workingPathToRecource];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Directory %@ has not been created %@", documentsDirectory, error);
        }
        
        if (!bundlePathToRecource) {
            NSData *data = [NSPropertyListSerialization dataWithPropertyList:@{} format:NSPropertyListXMLFormat_v1_0 options:NSPropertyListWriteStreamError error:&error];
            if (error)
                NSLog(@"MEContextArchiver error: %@", error);
            else
                [fileManager createFileAtPath:workingPathToRecource contents:data attributes:nil];
        }
        else if (![fileManager fileExistsAtPath: workingPathToRecource]){
            [fileManager copyItemAtPath:bundlePathToRecource toPath: workingPathToRecource error:&error];
            if (error) {
                NSLog(@"File %@ has not been copied to %@. %@", bundlePathToRecource, workingPathToRecource, error);
            }
        }
        
        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:workingPathToRecource];
    }
}


- (void) saveObject:(MEObject*)object withContextName:(NSString*)name error:(out NSError**) error{
    
    @synchronized(self){
        
        NSDictionary *dict = [object toDictionary];
        
        [dictionary setObject:dict forKey:name];
        
        NSError *__error;
        NSData *data = [NSPropertyListSerialization dataWithPropertyList:dictionary format:NSPropertyListBinaryFormat_v1_0 options:0 error:&__error];
        
        if (__error) {
            NSLog(@"MEContextArchiver: error %@", __error);
        }
        
        if (data) {
//            NSTimeInterval t1=[NSDate timeIntervalSinceReferenceDate];
            [data writeToFile:workingPathToRecource options:NSDataWritingAtomic error:error];
//            NSTimeInterval t2=[NSDate timeIntervalSinceReferenceDate];
//#if DEBUG
//            //NSLog(@"MEContextArchiver:saveObject:withContextName:error: saving context: %@ time=%fsec. bytes:%fKb rate:%fMb/sec.", name, (t2-t1), ([data length]/1024.), ([data length]/1024./1024.)/(t2-t1));
//#endif
        }
        else {
            *error = [NSError errorWithDomain:@"moex.com.archiver" code:1
                                     userInfo:@{
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Property list invalid format", @"Property list invalid for format"),
                                                NSLocalizedDescriptionKey: [NSString  stringWithFormat: NSLocalizedString(@"property list invalid format", @"")]
                                                }];
        }
    }
}

- (MEObject*) restoreObject:(MEObject*)object withContextName:(NSString*)name withLastTimeSave:(NSTimeInterval*)timeInterval{
    @synchronized(self){
        
        NSDictionary *object_data = [dictionary objectForKey:name];
        NSNumber *tm = [object_data valueForKey:@"__KEY_TIMESTAMP__"];
        *timeInterval = [tm doubleValue];
        if (object) {
            [object fromDictionary:object_data];
            return object;
        }
        else
            return [MEObject fromDictionary:object_data withInitObject:nil];
    }
}

- (MEObject*) restoreObject:(MEObject*)object withContextName:(NSString*)name ifLastSaveIsUptoday:(NSInteger)days{
    @synchronized(self){
        
        NSDictionary *object_data = [dictionary objectForKey:name];
        
        NSNumber *tm = [object_data valueForKey:@"__KEY_TIMESTAMP__"];
        NSTimeInterval lastSaveTime = [tm doubleValue];
        if ([self howManyDaysDifferenceBetween:[NSDate date] and:[NSDate dateWithTimeIntervalSince1970:lastSaveTime]]>days) {
            return object;
        }
        
        if (object) {
            [object fromDictionary:object_data];
            return object;
        }
        else
            return [MEObject fromDictionary:object_data withInitObject:nil];
    }
}


- (NSInteger) howManyDaysDifferenceBetween:(NSDate*)startDate and:(NSDate*)endDate
{
    NSDate * firstMidnight = [self getMidnightDateFromDate: startDate];
    NSDate * secondMidnight = [self getMidnightDateFromDate: endDate];
    NSTimeInterval timeBetween = [firstMidnight timeIntervalSinceDate: secondMidnight];
    
    NSInteger numberOfDays = ((timeBetween+.5) / 86400.);
    
    return ABS(numberOfDays);
}

- (NSDate *) getMidnightDateFromDate:(NSDate *)originalDate
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:originalDate];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *midnight = [[NSCalendar currentCalendar] dateFromComponents:components];
    return midnight;
}
@end
