//
//  MEObject.h
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEContextCoding.h"

#define MEOBJECT_DEBUGG 0

/**
 *  This is a base class to use MEContextCoding model patern.
 */
@interface MEObject : NSObject <MEContextCoding>

/**
 *  Get last time when object was saved.
 *
 *  @return seconds from the beginning of unix creation.
 */
- (NSTimeInterval) getLastSaveTime;

/**
 *  Dictionary of object properties.
 *
 *  @param obj object.
 *
 *  @return keyed dictionary.
 */
- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj;

/**
 *  Dictionary of object properties the object or super of the object.
 *
 *  @param obj       object.
 *  @param withSuper explore super of the object.
 *
 *  @return keyed dictionary.
 */
- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj withSuperClassProperties:(BOOL) withSuper;

/**
 *  Update or create new object from keyed properties dictionary.
 *
 *  @param obj        object or nil.
 *  @param dictionary keyed properties dictionary.
 *
 *  @return object.
 */
- (id) object:(id)obj fromDictionaryOfProperties:(NSDictionary *)dictionary;

/**
 *  Update or create new object from keyed properties dictionary of super.
 *
 *  @param obj        object or nil.
 *  @param dictionary keyed properties dictionary.
 *  @param withSuper explore super of the object.
 *
 *  @return object.
 */
- (id) object:(id)obj fromDictionaryOfProperties:(NSDictionary *)dictionary withSuperClassProperties:(BOOL) withSuper;

/**
 *  Create new or update from initial object.
 *
 *  @param dictionary dictionary.
 *  @param object     initial object or nil.
 *
 *  @return object.
 */
+ (id) fromDictionary: (NSDictionary*)dictionary withInitObject:(id)object;

/**
 *  Get property list names
 *
 *  @return NSString names.
 */
- (NSArray*) propertyList;

@end
