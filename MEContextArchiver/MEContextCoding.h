//
//  MEContextCoding.h
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *
     MEContextCoding/MEContextArchiver offer simplest way to reduce writing code lines for new class hierarchy or model which plans to save their context on disk.
     
     NSCoding is a great Apple standard library to save context of objects instead of using Core Data when Core Data has excessive functionality. 
     In other words: Not all apps need to query data. Not all apps need automatic migrations. Not all apps work with large or
     complex object graphs. NSCoding is a simple protocol but this simplicity leads to mostly boilerplate coding.
     Another issue applying NSCoding style is no way to update object state, only create new object.
     
     With MEContextCoding you can just inherit your class from MEObject instead of NSObject and some time overwrite two protocol methods: @see toDictionary and @see fromDictionary.
     To solve updating object context problem we can create new object and updates it calls @see fromDictionary.
     Methods can be manually rewritten or basically can be uses as is. As default MEObject explores the object properties and convert theirs to dictionary.
     One more complex way is when class inherited from another class inherited MEObject you should concatenate "super" and "self" properties dictionary.
 *
 */
@protocol MEContextCoding <NSObject>
/**
 *  Convert object properties to keyed dictionary.
 *
 *  @return dictionary.
 */
- (NSDictionary*) toDictionary;

/**
 *  Update object from dictionary.
 *
 *  @param dictionary keyed dictionary properties.
 */
- (void) fromDictionary: (NSDictionary*)dictionary;

@optional
/**
 *  Which properties should not coding.
 *
 *  @return dictionary
 */
- (NSDictionary*) excludePropertyNames;

@end
