//
//  MEObject.m
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEObject.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation MEObject
{
    NSTimeInterval lastSaveTime;
}

- (NSDictionary*) toDictionary{
    return [self dictionaryWithPropertiesOfObject:nil];
}

- (void) fromDictionary:(NSDictionary *)dictionary{
    [self object:self fromDictionaryOfProperties:dictionary];
}

+ (id) fromDictionary: (NSDictionary*)dictionary withInitObject:(id)object{
    MEObject *o = [[MEObject alloc] init];
    return [o object:nil fromDictionaryOfProperties:dictionary];
}

const char * property_getTypeString( objc_property_t property )
{
    const char * attrs = property_getAttributes( property );
    if ( attrs == NULL )
    return ( NULL );
    
    static char buffer[256];
    const char * e = strchr( attrs, ',' );
    if ( e == NULL )
    return ( NULL );
    
    int len = (int)(e - attrs);
    memcpy( buffer, attrs, len );
    buffer[len-1] = '\0';
    
    return ( &buffer[3] );
}

- (id) object:(id)obj fromDictionaryOfProperties:(NSDictionary *)dictionary{
    return [self object:obj fromDictionaryOfProperties:dictionary withSuperClassProperties:NO];
}

- (NSArray*) propertyList{
    unsigned count;
    objc_property_t *properties;
    
    properties = class_copyPropertyList([self class], &count);
    
    if (count==0)
        return nil;
    
    NSMutableArray *list = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        struct objc_property *property = properties[i];
        [list addObject: [NSString stringWithUTF8String:property_getName(property)]];
    }

    return list;
}

- (id) object:(id)obj fromDictionaryOfProperties:(NSDictionary *)dictionary withSuperClassProperties:(BOOL) withSuper{
    
    NSString *classObjectName = [dictionary valueForKey:@"__KEY_CLASSNAME__"];
    if (!classObjectName)
    return obj;
    
    if (self == obj || !obj) {
        NSNumber *tm = [dictionary valueForKey:@"__KEY_TIMESTAMP__"];
        lastSaveTime = [tm doubleValue];
    }
    
    Class classObject = NSClassFromString(classObjectName);
    
    id value = [dictionary valueForKey:@"__KEY_OBJECTVALUE__"];
    if (value){
        if ([classObject conformsToProtocol:@protocol(MEContextCoding)]) {
            if (!obj)
            obj = [[classObject alloc] init];
            
            [((MEObject*)obj) fromDictionary:value];
            return obj;
        }
        else
        return value;
    }
    
    
    if (!obj) {
        obj = [[classObject alloc] init];
        if ([obj conformsToProtocol:@protocol(MEContextCoding)]) {
            [((MEObject*)obj) fromDictionary:dictionary];
            return obj;
        }
    }
    
    unsigned count;
    objc_property_t *properties;
    
    if (withSuper)
    properties = class_copyPropertyList([obj superclass], &count);
    else
    properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        @try {
            struct objc_property *property = properties[i];
            
            NSString *key = [NSString stringWithUTF8String:property_getName(property)];
            
            if ([obj conformsToProtocol:@protocol(MEContextCoding)]) {
                if ([obj respondsToSelector:@selector(excludePropertyNames)]) {
                    NSDictionary *excludes = [obj performSelector:@selector(excludePropertyNames)];
                    if ([excludes valueForKey:key]) {
                        continue;
                    }
                }
            }
            
            const char *propertyType = property_getTypeString(property);
            Class classElementObject = NSClassFromString([NSString stringWithUTF8String: propertyType]);
            
            if ([classElementObject conformsToProtocol:@protocol(MEContextCoding) ]){
                
                //
                // initialize object property from dictionary if object conforms to protocol
                //
                NSDictionary *value_data = [dictionary objectForKey:key];
                if (value_data) {
                    id elementObject = [[classElementObject alloc] init];
                    [elementObject fromDictionary:value_data];
                    
                    NSString *selector = [NSString stringWithFormat:@"set%@%@:",[[key substringToIndex:1] capitalizedString],[key substringFromIndex:1]];
                    
                    //if ([obj respondsToSelector:NSSelectorFromString(selector)]) {
                        void (*response)(id, SEL, id) = (void (*)(id, SEL, id)) objc_msgSend;
                        SEL setSelector = sel_registerName([selector UTF8String]);
                        response(obj, setSelector, elementObject);
                    //}
                }
            }
            else {
                id value = [dictionary valueForKey:key];
                
                NSString *valueClassName = [value valueForKey:@"__KEY_CLASSNAME__"];
                Class valueClassObject = NSClassFromString(valueClassName);
                
                if ([valueClassObject isSubclassOfClass:[NSArray class]]) {
                    id array_value = [[valueClassObject alloc] init];
                    NSArray *value_data = [value valueForKey:@"__KEY_OBJECTVALUE__"];
                    for (id a in value_data) {
                        [array_value addObject:[self object:nil fromDictionaryOfProperties:a]];
                    }
                    [obj setValue: array_value forKey:key];
                }
                else if ([valueClassObject isSubclassOfClass:[NSDictionary class]]){
                    id _value = [[valueClassObject alloc] init];
                    NSArray *value_keys = [value valueForKey:@"__KEY_KEYS__"];
                    NSArray *value_dict = [value valueForKey:@"__KEY_OBJECTVALUE__"];
                    for (id k in value_keys) {
                        NSString *key_as_string = [k description];
                        [_value setObject:[self object:nil fromDictionaryOfProperties:[value_dict valueForKey:key_as_string]] forKey:k];
                    }
                    [obj setValue:_value forKey:key];
                }
                else if (value){
                    id object_value = [value valueForKey:@"__KEY_OBJECTVALUE__"];
                    [obj setValue:object_value forKey:key];
                }
                
            }
        }
        @catch (NSException *exception) {
#if MEOBJECT_DEBUGG
            NSLog(@"MEObject fromDictionary exception: %@ %s:%i", exception, __FILE__, __LINE__);
#endif
        }
    }
    
    free(properties);
    
    return obj;
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj{
    return [self dictionaryWithPropertiesOfObject:obj withSuperClassProperties:NO];
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj withSuperClassProperties:(BOOL) withSuper{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (obj==nil)
        obj = self;
    
    
    unsigned count;
    objc_property_t *properties;
    
    if (withSuper)
    properties = class_copyPropertyList([obj superclass], &count);
    else
    properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        
        @try {
            struct objc_property *property = properties[i];
            
            NSString *key = [NSString stringWithUTF8String:property_getName(property)];
            
            if ([obj conformsToProtocol:@protocol(MEContextCoding)]) {
                if ([obj respondsToSelector:@selector(excludePropertyNames)]) {
                    NSDictionary *excludes = [obj performSelector:@selector(excludePropertyNames)];
                    if ([excludes valueForKey:key]) {
                        continue;
                    }
                }
            }
            
            const char *propertyType = property_getTypeString(property);
            Class classObject = NSClassFromString([NSString stringWithUTF8String: propertyType]);
            
            if ([classObject conformsToProtocol:@protocol(MEContextCoding) ]){
                id value = [obj valueForKey:key];
                value = [value performSelector:@selector(toDictionary)];
                if (value) {
                    [dict setObject:value forKey:key];
                }
            }
            else if ([classObject isSubclassOfClass:[NSDictionary class]]){
                id value = [obj valueForKey:key];
                
                if (![value respondsToSelector:@selector(keyEnumerator)]) {
#if MEOBJECT_DEBUGG
                    NSLog(@"MEObject error: class %@ of the object [%@] is not compatible with the storage... for key: %@, %s:%i", [value class], value, key, __FILE__, __LINE__);
#endif
                    continue;
                }
                
                NSArray *keys = [[value keyEnumerator] allObjects];
                id dicted_dict = [[classObject alloc] init];
                
                if (value) {
                    for (id k in value) {
                        id new_object = [value objectForKey:k];
                        id new_value;
                        NSString *key_as_string = [k description];
                        
                        if ([new_object conformsToProtocol:@protocol(MEContextCoding)]){
                            new_value = [new_object toDictionary];
                            if (new_value) {
                                [dicted_dict setObject:@{
                                                         @"__KEY_CLASSNAME__": NSStringFromClass([new_object class]),
                                                         @"__KEY_OBJECTVALUE__": new_value
                                                         }
                                                forKey:key_as_string];
                            }
                        }
                        else{
                            new_value = [self dictionaryWithPropertiesOfObject:new_object];
                            if (new_value)
                                [dicted_dict setObject:new_value forKey:key_as_string];
                        }
                    }
                    
                    
                    [dict setObject: @{
                                       @"__KEY_KEYS__": keys,
                                       @"__KEY_CLASSNAME__": NSStringFromClass(classObject),
                                       @"__KEY_OBJECTVALUE__": dicted_dict
                                       }
                             forKey:key
                     ];
                }
            }
            else if ([classObject isSubclassOfClass:[NSArray class]]) {
                id value = [obj valueForKey:key];
                if (value) {
                    id dicted_array = [[classObject alloc] init];
                    for (id a in value) {
                        if (a){
                            if ([a conformsToProtocol:@protocol(MEContextCoding)]){
                                id v = [a toDictionary];
                                if (v)
                                    [dicted_array addObject:@{
                                                              @"__KEY_CLASSNAME__": NSStringFromClass([a class]),
                                                              @"__KEY_OBJECTVALUE__": v
                                                              }];
                            }
                            else
                                [dicted_array addObject:[self dictionaryWithPropertiesOfObject:a]];
                        }
                        else
                            [dicted_array addObject:[NSNull null]];
                    }
                    [dict setObject: @{
                                       @"__KEY_CLASSNAME__": NSStringFromClass([value class]),
                                       @"__KEY_OBJECTVALUE__": dicted_array
                                       }
                             forKey:key
                     ];
                }
            }
            else{
                id value = [obj valueForKey:key];
                
                if (value) {
                    if ([value isKindOfClass:[NSString class]]
                        ||
                        [value isKindOfClass:[NSNumber class]]
                        ) {
                        
                        [dict setObject:@{
                                          @"__KEY_CLASSNAME__": NSStringFromClass([value class]),
                                          @"__KEY_OBJECTVALUE__": value
                                          }
                                 forKey:key];
                    }
                    else if ([value isKindOfClass:[MEObject class]]){
                        [self dictionaryWithPropertiesOfObject:value withSuperClassProperties:YES];
                    }
                    else {
#if MEOBJECT_DEBUGG
                        NSLog(@"MEObject error: class %@ of the object [%@] is not compatible with the storage..., %s:%i", [value class], value, __FILE__, __LINE__);
#endif
                    }
                }
                
            }
        }
        @catch (NSException *exception) {
            NSLog(@"MEObject error: %@ %s:%i", exception, __FILE__, __LINE__);
        }
    }
    
    free(properties);
    
    [dict setObject:NSStringFromClass([obj class]) forKey:@"__KEY_CLASSNAME__"];
    if (count==0)
    [dict setObject:obj forKey:@"__KEY_OBJECTVALUE__"];
    else
    [dict setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"__KEY_TIMESTAMP__"];
    
    return [NSDictionary dictionaryWithDictionary:dict];
}


- (NSTimeInterval) getLastSaveTime{
    return lastSaveTime;
}

@end
