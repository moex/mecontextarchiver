//
//  MEContextArchiver.h
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEContextCoding.h"
#import "MEObject.h"

#define ME_DEFAULT_CONTEXT_PLIST_NAME @"MEContextArchiver.plist"


/**
 *  Create context archiver.
 */
@interface MEContextArchiver : NSObject

/**
 *  Create shared instance and initialize store file with name. Only one stora can be used in one app.
 *  
 *  @param fileName file name in document catalog.
 *  
 *  @return context archiver.
 */
+ (id) sharedInstanceWithFile:(NSString*)fileName;

/**
 *  Create instance or get already create context archiver.
 *  
 *  @return context archiver.
 */
+ (id) sharedInstance;

/**
 *  Save an object context. The object should conform the @see MECoding protocol. ALl prperties of the object should be standart NSObject or also conform the @see MECoding protocol.
 *  
 *  @param object object.
 *  @param name name.
 *  @param error nil if all is ok, or NSError indicate what issue happened. 
 */
- (void) saveObject:(MEObject*)object withContextName:(NSString*)name error:(out NSError**) error;

/**
 *  Restore an object context to the object state with certain context name.
 *  
 *  @param object       object.
 *  @param name         context name.
 *  @param timeInterval time when object was saved.
 *  
 *  @return reference to the object or nil if bject could not be restored.
 */
- (MEObject*) restoreObject:(id)object withContextName:(NSString*)name withLastTimeSave:(NSTimeInterval*)timeInterval;

/**
 *  Restore an object context to the object state with certain context name in case if the context is fresh.
 *  
 *  @param object object.
 *  @param name   name.
 *  @param days   days from last save.
 *  
 *  @return object or nil.
 */
- (MEObject*) restoreObject:(id)object withContextName:(NSString*)name ifLastSaveIsUptoday:(NSInteger)days;

@end
