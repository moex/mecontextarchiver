# MEContextArchiver

Created by **denn.nevera**

 MEContextCoding/MEContextArchiver offer simplest way to reduce writing code lines for new class hierarchy or model which plans to save their context on disk.

 NSCoding is a great Apple standard library to save context of objects instead of using Core Data when Core Data has excessive functionality. 
 In other words: Not all apps need to query data. Not all apps need automatic migrations. Not all apps work with large or complex object graphs. 
 NSCoding is a simple protocol but this simplicity leads to mostly boilerplate coding. Another issue applying NSCoding style is no way to update object state, only create new object.

 With MEContextCoding you can just inherit your class from MEObject instead of NSObject and sometimes overwrite two protocol methods:  toDictionary and fromDictionary.
 To solve updating object context problem we can create new object and updates it calls fromDictionary.
 Methods can be manually rewritten or basically can be uses as is. As default MEObject explores the object properties and convert their to keyed properties dictionary.
 One more complex way is when class inherits another class inherited MEObject, in this case you should concatenate "super" and "self" properties dictionary.