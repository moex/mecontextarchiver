//
//  MEContextArchiverTests.m
//  MEContextArchiverTests
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MEContextArchiver.h"
#import "METestClasses.h"


@interface MEContextArchiverTests : XCTestCase

@end

@implementation MEContextArchiverTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testClass1
{
    
    METestClass1 *class1 = [[METestClass1 alloc] init];
}

- (void)testClass2
{
    //XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
        
    METestClass2 *class2 = [[METestClass2 alloc] init];
    
    [class2 fill];
    
    METestClass2 *classToRestore = [[METestClass2 alloc] init];
    
    [classToRestore fromDictionary:[class2 toDictionary]];
}

- (void)testClass3{
    
    METestClass3 *class3 = [[METestClass3 alloc] init];
    
    [class3 fill];
    
    METestClass3 *classRestore3 = [[METestClass3 alloc] init];

    [classRestore3 fromDictionary:[class3 toDictionary]];
}

- (void)testArchive1{
    MEContextArchiver *archiver = [MEContextArchiver sharedInstance];
    
    METestClass3 *class3 = [[METestClass3 alloc] init];
    [class3 fill];
    
    NSError *error;
    [archiver saveObject:class3 withContextName:@"class3" error:&error];
    
    if (error) {
        //NSLog(@"Error: %@", error);
        XCTFail(@"Error : %@ in \"%s\"", error, __PRETTY_FUNCTION__);
        return;
    }
    
    NSTimeInterval interval;
    METestClass3 *testRestoreClass3 = (METestClass3*)[archiver restoreObject:nil withContextName:@"class3" withLastTimeSave:&interval];
    NSLog(@"METestClass3 testRestoreClass3: %@", [testRestoreClass3 toDictionary]);    
}

@end
