//
//  METestClasses.h
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEObject.h"

@interface METestClass1 : MEObject
@property(nonatomic,strong) NSString *one;
@property(nonatomic,strong) NSString *two;
@property(nonatomic) NSInteger integer;
@property(nonatomic) NSNumber *number;
@property(nonatomic) BOOL hasSome;
@property(nonatomic,strong,readonly) NSString *readonly;

- (void) fill;

@end

@protocol METestProtocol <NSObject>
- (void) method;
@end


@interface METestClass2 : MEObject
@property(nonatomic,strong) METestClass1 *class1;
@property(nonatomic,strong) NSMutableDictionary *dict;
@property(nonatomic,strong) NSMutableArray *array;
@property(nonatomic,strong) NSMutableArray *numbers;

- (void) fill;

@property(nonatomic,strong) id<METestProtocol> delegate;

@end


@interface METestClass3 : METestClass2 <METestProtocol>
@property(nonatomic,strong) NSString *name;
@end