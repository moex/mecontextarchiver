//
//  METestClasses.m
//  MEContextArchiver
//
//  Created by denis svinarchuk on 15.12.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "METestClasses.h"

@interface METestClass1()
@property(nonatomic,strong) NSString *readonly;
@end

@implementation METestClass1

- (id) init{
    
    self = [super init];
    
    return self;
}

- (void) fill{
    _one = @"1";
    _two = @"2";
    _hasSome = YES;
    self.readonly = @"readonly";
}

@end


@implementation METestClass2

- (void) fill{
    _class1 = [[METestClass1 alloc] init];
    _array = [[NSMutableArray alloc] init];
    _numbers = [[NSMutableArray alloc] init];
    _dict = [[NSMutableDictionary alloc] init];
    
    for (int i=0; i<5; i++) {
        [_numbers addObject:[NSNumber numberWithInt:i]];
    }
    
    for (int i=0; i<5; i++) {
        METestClass1 *c1 = [[METestClass1 alloc] init];
        c1.one = [NSString stringWithFormat:@"%i",i];
        c1.two = [NSString stringWithFormat:@"%i+1",i];
        [_array addObject:c1];
    }
    
    for (int i=0; i<5; i++) {
        METestClass1 *c1 = [[METestClass1 alloc] init];
        [_dict setObject:c1 forKey:[NSString stringWithFormat:@"%i", i]];
    }
}

- (id) init{
    self = [super init];
    return self;
}

@end

@implementation METestClass3

- (void) method{
    NSLog(@"Method");
}

- (id) init{
    self = [super init];
    
    _name = @"METestClass3";
    self.delegate = self;
    
    return self;
}

- (NSDictionary*) toDictionary{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super dictionaryWithPropertiesOfObject:self withSuperClassProperties:YES]];
    NSMutableDictionary *selfdict = [NSMutableDictionary dictionaryWithDictionary:[self dictionaryWithPropertiesOfObject:self withSuperClassProperties:NO]];
    
    [dict addEntriesFromDictionary:selfdict];
    
    return dict;
}

- (void) fromDictionary:(NSDictionary *)dictionary{
    [super object:self fromDictionaryOfProperties:dictionary withSuperClassProperties:YES];
    [super object:self fromDictionaryOfProperties:dictionary withSuperClassProperties:NO];
}

- (NSDictionary*) excludePropertyNames{
    return @{@"delegate": @NO};
}

@end